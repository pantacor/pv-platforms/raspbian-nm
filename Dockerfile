FROM registry.gitlab.com/pantacor/pantavisor-runtime/pvlogger:ARM32V6-master as pvlogger

FROM balenalib/rpi-raspbian:buster

COPY --from=pvlogger /usr/local/bin/pvlogger /usr/local/bin/

RUN apt-get update \
	&& apt-get install -y \
		network-manager \
		openssh-server \
		rsyslog \
		logrotate \
		vim-tiny \
		wireless-regdb \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists \
	&& ln -s /lib/systemd/system/logrotate.timer /etc/systemd/system/timers.target.wants/logrotate.timer \
	&& useradd -ms /bin/bash pi \
	&& echo "pi:pantacor" || chpasswd \
	&& echo "root:pantacor" || chpasswd \
	&& sed -i 's/#ForwardToSyslog=yes/ForwardToSyslog=yes/' /etc/systemd/journald.conf \
	&& sed -i 's/#Port 22/Port 7025/;s/.*PermitRootLogin.*$/PermitRootLogin yes/;/.*PasswordAuthentication.*/d;$aPasswordAuthentication yes\n' /etc/ssh/sshd_config

RUN mkdir -p /etc/systemd/system/multi-user.target.wants/ || true \
        && ln -s /etc/systemd/regenerate_ssh_host_keys.service /etc/systemd/system/multi-user.target.wants/ \
        && ln -s /etc/systemd/system/pvlogger-syslog.service /etc/systemd/system/multi-user.target.wants/ \
        && rm -f /etc/systemd/system/timers.target.wants/apt-daily-upgrade.timer \
        && rm -f /etc/systemd/system/timers.target.wants/apt-daily.timer

CMD [ "/bin/systemd" ]

